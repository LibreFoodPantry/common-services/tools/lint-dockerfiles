#!/bin/sh

FILES="$(find . -type f \
    -not \( -path '*/node_modules/*' -o -path '*/.git/*' \) \
    \( -iname 'Dockerfile' -o -iname '*.Dockerfile' -iname '*.df' \) )"


if [ -z "$FILES" ] ; then
    echo "No Dockerfiles to lint. Skipping."
else
    echo "Linting:"
    echo "$FILES"
    echo "$FILES" | xargs -n 1 /bin/hadolint
fi
