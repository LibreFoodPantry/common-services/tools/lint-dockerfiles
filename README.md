# Lint Dockerfiles

Recursively runs [hadolint](https://github.com/hadolint/hadolint) on all
Dockerfiles in a given directory.

## Use locally

```bash
docker run --rm \
    -v "${PWD}:/work:ro" -w /work \
    registry.gitlab.com/librefoodpantry/common-services/tools/lint-dockerfiles:latest
```

## Use in CI

Best used with [Pipeline](https://gitlab.com/LibreFoodPantry/common-services/tools/pipeline).

Otherwise, include source/gitlab-ci/lint-dockerfiles.yml in your .gitlab-ci.yml
file.

If you don't have any Dockerfiles, you can disable this job by setting
`LINT_DOCKERFILES_ENABLED` to `"false"` in your .gitlab-ci.yml file.

```yaml
varilabes:
  LINT_DOCKEFILES_ENABLED: "false"
```

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
